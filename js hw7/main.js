let peopleList = [{
    name: "Ivan",
    surname: "Ivanov",
    gender: "male",
    age: 30
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 22
},
{
    name: "Sergei",
    surname: "Ivanovs",
    gender: "female",
    age: 30
},
{
    name: "Egor",
    surname: "Ivanova",
    gender: "female",
    age: 22
},
{
    name: "Max",
    surname: "Ivanova",
    gender: "female",
    age: 22
}]

let excluded = [{
    name: "Ivan",
    surname: "Ivanov",
    gender: "male",
    age: 30
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 22
},
{
    name: "Kostya",
    surname: "Ivanovs",
    gender: "female",
    age: 30
}]

function excludeBy(peopleList, excluded, names) {
    let result = []
    for (let i = 0; i < peopleList.length; i++) {
        let sum = false;
        for (let b = 0; b < excluded.length; b++) {
            if (peopleList[i][names] == peopleList[b][names]) {
                sum = true;
            }
        }
        if (!sum) {
            result.push(peopleList[i]);
        }
    }
    return result;
};
console.log(excludeBy(peopleList, excluded, 'name'));